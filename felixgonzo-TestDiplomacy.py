from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_eval, diplomacy_solve


class TestDiplomacy(TestCase):

    # --------------
    # diplomacy_read
    # --------------
    def test_read_1(self):
        r = " "
        w = diplomacy_read(r)
        self.assertEqual(w, [])

    def test_read_2(self):
        r = "B Austin Move Madrid"
        w = diplomacy_read(r)
        self.assertEqual(w, ["B", "Austin", "Move", "Madrid"])

    def test_read_3(self):
        r = "A Dallas Support D"
        w = diplomacy_read(r)
        self.assertEqual(w, ["A", "Dallas", "Support", "D"])

    # --------------
    # diplomacy_eval
    # --------------
    def test_eval_1(self):
        input = [["A", "Madrid", "Support", "C"],
                 ["B", "Austin", "Move", "Madrid"],
                 ["C", "London", "Move", "NewYork"],
                 ["D", "Houston", "Move", "Austin"]]
        output = diplomacy_eval(input)
        self.assertEqual(
            output, {'A': '[dead]', 'B': '[dead]', 'C': 'NewYork', 'D': 'Austin'})

    def test_eval_2(self):
        input = [["A", "Dallas", "Move", "Houston"]]
        output = diplomacy_eval(input)
        self.assertEqual(output, {'A': 'Houston'})

    def test_eval_3(self):
        input = [["A", "Cartagena", "Hold"],
                 ["B", "Dubai", "Support", "A"],
                 ["C", "Jaipur", "Support", "B"],
                 ["D", "Salzburg", "Move", "Dubai"],
                 ["E", "SaintPetersburg", "Move", "Cartagena"]]
        output = diplomacy_eval(input)
        self.assertEqual(output, {
                         'A': '[dead]', 'B': 'Dubai', 'C': 'Jaipur', 'D': '[dead]', 'E': '[dead]'})

    # --------------
    # diplomacy_print
    # --------------
    def test_print_1(self):
        w = StringIO()
        input = {'A': '[dead', 'B': '[dead]', 'C': 'NewYork', 'D': 'Austin'}
        output = diplomacy_print(w, input)
        self.assertEqual(
            w.getvalue(), 'A [dead\nB [dead]\nC NewYork\nD Austin\n')

    def test_print_2(self):
        w = StringIO()
        input = {'A': '[dead]', 'B': 'Dubai',
                 'C': 'Jaipur', 'D': '[dead]', 'E': '[dead]'}
        output = diplomacy_print(w, input)
        self.assertEqual(
            w.getvalue(), 'A [dead]\nB Dubai\nC Jaipur\nD [dead]\nE [dead]\n')

    def test_print_3(self):
        w = StringIO()
        input = {'A': '[dead]', 'B': '[dead]', 'C': '[dead]'}
        output = diplomacy_print(w, input)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\n')

    # --------------
    # diplomacy_solve
    # --------------
    def test_solve_1(self):
        r = StringIO("A Dubai Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Dubai\n")

    def test_solve_2(self):
        r = StringIO(
            "A Skopje Support C\nB Austin Move Skopje\nC NewOrleans Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC NewOrleans\n")

    def test_solve_3(self):
        r = StringIO(
            "A Palermo Hold\nB Odessa Move Palermo\nC Amsterdam Support B\nD Yokohama Move Amsterdam")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_4(self):
        r = StringIO(
            "A Sarajevo Hold\nB Prague Move Sarajevo\nC Istanbul Move Algiers\nD Algiers Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_5(self):
        r = StringIO(
            "A Memphis Hold\nB Honolulu Move Memphis\nC Wellington Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Memphis\nC Wellington\n")


if __name__ == "__main__":  # pragma: no cover
    main()
