#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve, diplomacy_print, diplomacy_eval, Army


# -----------
# TestCollatz
# -----------
class TestDiplomacy(TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval([Army("A", "Madrid", "Hold")])
        self.assertEqual(v[0].name, "A")
        self.assertEqual(v[0].location, "Madrid")

    def test_eval_2(self):
        v = diplomacy_eval([Army("A", "Madrid", "Hold"),
                            Army("B", "Barcelona", "Move", "Madrid")])
        self.assertEqual(v[0].name, "A")
        self.assertEqual(v[0].location, "[dead]")
        self.assertEqual(v[1].name, "B")
        self.assertEqual(v[1].location, "[dead]")

    def test_eval_3(self):
        v = diplomacy_eval([Army("A", "Madrid", "Hold"),
                            Army("B", "Barcelona", "Move", "Madrid"),
                            Army("C", "Austin", "Support", "A")])
        self.assertEqual(v[0].name, "A")
        self.assertEqual(v[0].location, "Madrid")
        self.assertEqual(v[1].name, "B")
        self.assertEqual(v[1].location, "[dead]")
        self.assertEqual(v[2].name, "C")
        self.assertEqual(v[2].location, "Austin")

    # -----
    # solve
    # -----
    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        test_list = [Army("A", "Madrid", "Hold")]
        diplomacy_print(w, test_list)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_2(self):
        w = StringIO()
        test_list = [Army("A", "Madrid", "Hold"),
                     Army("B", "Barcelona", "Move", "Madrid")]
        diplomacy_print(w, test_list)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\n")

    def test_print_3(self):
        w = StringIO()
        test_list = [Army("A", "Madrid", "Hold"),
                     Army("B", "Barcelona", "Move", "Madrid"),
                     Army("C", "Austin", "Support", "A")]
        diplomacy_print(w, test_list)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC Austin\n")


# ----
# main
# ----


if __name__ == "__main__":
    main()
