#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  2 14:32:37 2022

@author: ritikachopra
"""

from io import StringIO
import sys
from unittest import main, TestCase

from Diplomacy import diplomacy_solve, diplomacy_eval, diplomacy_print, diplomacy_read

class TestDiplomacy(TestCase):
    
    def test_eval_1(self):
        r = ["A Madrid Move Barcelona", "B Barcelona Move Madrid"]
        m = diplomacy_eval(r)
        
        self.assertEqual(m, ['A Barcelona', 'B Madrid'])
        
    def test_eval_2(self):
        r = ["A Denver Hold", "B Aspen Move Denver", "C Vail Support A", "D Boulder Move Vail"]
        m = diplomacy_eval(r)
        
        self.assertEqual(m, ['A [dead]', 'B [dead]', 'C [dead]', 'D [dead]'])
        
    def test_eval_3(self):
        r = ["A Delhi Hold", "B Agra Support A", "C Hyderabad Move Delhi"]
        m = diplomacy_eval(r)
        self.assertEqual(m, ['A Delhi', 'B Agra', 'C [dead]'])
        
    def test_solve_1(self): 
        r = StringIO("A Delhi Hold\nB Agra Support A\nC Hyderabad Move Delhi")
        w = StringIO()
        diplomacy_solve(r, w)
        tmp = w.getvalue()
        self.assertEqual(tmp, "A Delhi\nB Agra\nC [dead]\n")
        
    def test_solve_2(self): 
        r = StringIO("A Denver Hold\nB Aspen Move Denver\nC Vail Support A\nD Boulder Move Vail")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
        
    def test_solve_3(self): 
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB Madrid\n")
        
    def test_print_1(self): 
        w = StringIO()
        j = ["A [dead]", "B LasVegas", "C [dead]", "D SugarLand"]
        diplomacy_print(w, j)
        self.assertEqual(w.getvalue(), "A [dead]\nB LasVegas\nC [dead]\nD SugarLand\n")
        
    def test_print_2(self): 
        w = StringIO()
        j = ["A Jammu", "B Aurora", "C Austin", "D FlowerMound"]
        diplomacy_print(w, j)
        self.assertEqual(w.getvalue(), "A Jammu\nB Aurora\nC Austin\nD FlowerMound\n")

    def test_print_3(self): 
        w = StringIO()
        j = ["A Rajkot", "B Chennai", "C [dead]", "D Hyderabad", "E Bangalore", "F Sojitra"]
        diplomacy_print(w, j)
        self.assertEqual(w.getvalue(), "A Rajkot\nB Chennai\nC [dead]\nD Hyderabad\nE Bangalore\nF Sojitra\n")        
        
    def test_read_1(self): 
        j = StringIO("A Vail Hold\nB FlowerMound Support A\nC Pluto Move Vail")
        z = diplomacy_read(j)
        self.assertEqual(z, ["A Vail Hold", "B FlowerMound Support A", "C Pluto Move Vail"])
        
    def test_read_2(self): 
        j = StringIO("A Barcelona Hold\nB Baroda Support C\nC Aurora Move Barcelona\nD Plano Hold")
        z = diplomacy_read(j)
        self.assertEqual(z, ["A Barcelona Hold", "B Baroda Support C", "C Aurora Move Barcelona", "D Plano Hold"])
            
    def test_read_3(self): 
        j = StringIO("A Denver Hold\nB Aspen Move Denver\nC Vail Support A\nD Boulder Move Vail")
        z = diplomacy_read(j)
        self.assertEqual(z, ["A Denver Hold", "B Aspen Move Denver", "C Vail Support A", "D Boulder Move Vail"]) 
        
        

if __name__ == "__main__":
    main()
    
    
    
    
    